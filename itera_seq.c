#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <gmp.h>

//gcc itera_seq.c -lgmp


void f(mpf_t res, unsigned long from, unsigned long to){
  unsigned long i;
  
  mpf_init(res);
  mpf_t v0;
  mpf_init(v0);
  
  mpf_set_ui(res, 0); // res = 0
  
  for(i = from; i < to; i += 2){
    mpf_set_ui(v0, i); // v0 = i
    mpf_pow_ui(v0, v0, 2); // v0 = i^2
    mpf_div_ui(v0, v0, 6); // v0 = (i^2)/6

    mpf_add(res, res, v0); // res = res + v0
  }
  
  mpf_clear(v0);
}

int main(int ac, char** av){
  int nb_proc = 4;
  int i;
  float elapse;
  unsigned long n = 25000000;
  unsigned long size = n / nb_proc;
  
  struct timeval initial_time;
  struct timeval final_time;
  
  mpf_t tmp;
  mpf_t res;
  
  gettimeofday(&initial_time, 0);
  
  mpf_init(res);
  mpf_set_ui(res, 0);
  
  f(res, 0, n);
  mpf_add(res, res, 3); 
   
  gettimeofday(&final_time, 0);
  elapse = ((float) (final_time.tv_sec - initial_time.tv_sec)) + ((float) (final_time.tv_usec - initial_time.tv_usec))/1000000.0;
  
  gmp_printf("%d (%fs) - res : %F.30f\n", i, elapse, res);
  mpf_clear(res);
  
  return 0;
}
